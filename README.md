# Debug Loop

WIP: Starting with a script to debug bootloops.

## Getting Started

Debugging bootloops While adb disabled is a headache to Developers. 

This is solution to reduce the pain of developers. [ That's fine Just breath :) ]

### Prerequisites
Mac or Linux Machine with ADB tools installed. 

Device with Any ADB enabled Recovery. TWRP is just perfect

### HOWTO

Clone the repo

```
git clone https://gitlab.e.foundation/muppathimoonu/debug-loop.git
cd debug-loop/
chmod +x debug-loop.sh
```
Reboot your device to Recovery.

Fireup your terminal, Its time!

```
./debug-loop.sh
```
Thats it.

Extras: Recorded Log file can be found in project directory.

## KEEP IN MIND

** Run this script after firstboot only.

Reboot to TWRP Only after you see a bootloop, then run this script.



### Feature Plans: 
As of now, this will work on MAC & Linux. 

Sooner, will make this as a perfect Out of box solution and extend support to windows as well.
