#!/bin/bash
adb push ~/.android/adbkey.pub /data/misc/adb/adb_keys
adb reboot
while :
do
 status="$(adb get-state)"
    if [[ $status == "device" ]];
    then
      device="$(adb shell getprop ro.product.name)"
      echo Device found: $device
      adb logcat | tee "logcat-$device-$(date +"%Y_%m_%d_%I_%M_%p").log"
      break
      else
      echo "Waiting for device to reboot"
  fi
done